<?php

session_start();
require __DIR__ . '/../vendor/autoload.php';

// setup whoops
$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

class MyDbException extends Exception {
	public const DUP_KEY = 1062;

	public $errorNo;

	public function __construct(string $msg, int $errNo) {
		parent::__construct($msg);
		$this->errorNo = $errNo;
	}
}

function isLoggedIn() {
	return isset($_SESSION['user']);
}

function getResult(string $sql) {
	$conn = new mysqli('localhost','root','','encryptdb');
	$result = $conn->query($sql);

	if (!$result) {
		throw new MyDbException($conn->error, $conn->errno);
	}

	return $result;
}

function printError(string $msg, string $url = null) {
	if ($url) {
		$msg .= " <a href=\"$url\">Go back</a>";
	}

	echo "<p>$msg</p>";
}