drop database if exists encryptdb;
create database if not exists encryptdb;
use encryptdb;

create table users(
	`id` int unsigned auto_increment primary key,
	`username` nvarchar(255) not null,
	`password` char(32) not null,
	`created_date` datetime default current_timestamp,
	`last_updated` timestamp,
	unique(`username`)
);