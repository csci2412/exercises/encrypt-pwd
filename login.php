<?php

require_once 'include/functions.php';

class MyLoginException extends Exception {}

function handleSubmit() {
	try {
		$user = $_POST['username'];
		$pwd = $_POST['password'];
		
		if (empty($user) || empty($pwd)) {
			throw new MyLoginException('All fields are required');
		}

		$result = getResult("SELECT * FROM users WHERE username = '$user'");
		
		if ($result->num_rows !== 1) {
			throw new MyLoginException('Username not found');
		}

		$user = $result->fetch_object();

		if ($user->password !== md5($pwd)) {
			throw new MyLoginException('Invalid password');
		}

		// we made it!
		$_SESSION['user'] = $user->username;

		header('Location: secret-page.php');
	} catch (MyLoginException $e) {
		printError($e->getMessage(), 'index.php');
	} catch (Exception $e) {
		printError('Problem with registration', 'index.php');
	}
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	handleSubmit();
}