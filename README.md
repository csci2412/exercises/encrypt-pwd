# Steps to clone and get started:

 1. `cd` to somewhere in C:\xampp\htdocs
 2. `git clone [clone link copied from gitlab]`
 3. `cd` into project folder
 4. `composer install`
 5. In Workbench, open encryptdb.sql and execute the script