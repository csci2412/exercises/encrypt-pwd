<?php include 'include/header.php' ?>
<?php
	if (!isLoggedIn()) {
		header('Location: index.php');
		exit();
	}
?>
<nav>
	<p><a href="logout.php">Logout</a></p>
</nav>
<h1>Welcome, <?=$_SESSION['user']?>! You made it to the secret page!</h1>
<p>The Answer to the Ultimate Question of Life, The Universe, and Everything is 42</p>

<?php include 'include/footer.php' ?>