<?php

require_once 'include/functions.php';

class MyRegException extends Exception {}

function handleSubmit() {
	try {
		$user = $_POST['username'];
		$pwd = $_POST['password'];
		$confirm = $_POST['confirm'];

		if (empty($user) || empty($pwd) || empty($confirm)) {
			throw new MyRegException('All fields are required');
		}

		if ($pwd !== $confirm) {
			throw new MyRegException('Passwords do not match');
		}

		$result = getResult("INSERT users (username, password) 
			VALUES('$user', md5('$pwd'))");
		
		if ($result) {
			header('Location: index.php');
		}
	} catch (MyRegException $e) {
		printError("Problem with registration: {$e->getMessage()}", 'register.php');
	} catch (MyDbException $e) {
		if ($e->errorNo !== MyDbException::DUP_KEY) {
			throw new Exception($e->getMessage());
		}

		printError('Username already taken','register.php');
	} catch (Exception $e) {
		printError('Problem with registration', 'register.php');
	}
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	handleSubmit();
}